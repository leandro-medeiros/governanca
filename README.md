# Guia de boas práticas Front-End

_O intuito desse documento é trazer um conjunto de informações para transformar o ambiente de desenvolvimento prático e organizado, qualquer informação a mais poderá ser adicionada e/ou removida para que tenhamos uma boa experiência ao "botar a mão na massa"_

##Conteúdos abordados

1. [Airbnb Style Guide](#airbnb-style-guide)
1. [Estilização eficiente](#estilização-eficiente)
1. [Estrutura de pastas](#estrutura-de-pastas)
1. [Commits Eficientes](#commits-eficientes)

## Airbnb Style Guide

_As regras a seguir esta traduzida da documentação oficial, você poderá visualiza-las em [**documentação Airbnb**](https://github.com/airbnb/javascript/blob/master/react/README.md)_

- Inclua apenas um componente por arquivo.
- Porém, é permitido adicionar múltiplos [Stateless, ou Pure, Components](https://facebook.github.io/react/docs/reusable-components.html#stateless-functions) por arquivo. eslint: [`react/no-multi-comp`](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-multi-comp.md#ignorestateless).
- Sempre use a sintaxe JSX.
- Não use `React.createElement`, a menos que você esteja inicializando a aplicação em um arquivo que não seja JSX.

_As regras acima são de suma importância para um bom entendimento de criação de novos **components**_.

**Stateless Components** - Dê preferência em criar componentes com funções (`functions`) ao invés de _arrow functions_

```jsx
// bad
class MyComponent extends React.Component {
  render() {
    return <div>Um component sem estado</div>;
  }
}

// avoid
const MyComponent = props => <div>Um component sem estado</div>;

// good
function MyComponent() {
  return <div>Um component sem estado</div>;
}
```

**Statefull Components** - Sempre crie um Statefull component com class, evite usar `React.createClass`

```jsx
// bad
const MyComponent = React.createClass({
  render() {
    return <div>{Um component qualquer}</div>;
  }
});


// good
class MyComponent extends React.Component {
    render() {
        return <div>{Um component qualquer}</div>;
    }
}
```

**Fragmentação de components** - Sempre tenha em mente que um `Component` não pode ser **muito** extenso, opte pela fragmentação do mesmo (pensando na parte de reutilização).

```jsx

// bad
class Form extends React.Component {
  // ...

  render() {
    return (
      <div className="form-default">
        <div className="item">
          <label> { /* ... */} </label>
          <input type="text" onChange={/* ... */}>
        </div>
      </div>
    )
  }
}

// good

// components/Input/index.jsx
function Input(props) {
  return (
    <div className="item">
      <label> {/* ... */} </label>
      <input {...props}>
    </div>
  )
}

// containers/View/index.jsx
import Input from '../components/Input';

class ViewComponent extends React.Component {
  // ...
  render() {
    return (
      <form>
        <Input type="text" onChange={ /* ... */}/>
      </form>
    )
  }
}
```

**Para ver outras regras acesse [Airbnb React/JSX Style Guide](https://github.com/airbnb/javascript/blob/master/react/README.md)**
![Documentação Vídeo Airbnb](https://lh3.googleusercontent.com/urbOdyRpXdH8RQIQ3oEogfBkTND1tyh3NdYvnH1QENUqtzjHcBa1wkHHr6Z1SrgGe63KxGFH4oGDr4f5EWgLqUTFmSLS9rtKcvuIt4FaIYl-yvVXTa2ZtA4ac6yXBq5WwU0CJ28VG-8sd54pd8mtnWJLOLB_lX-OKptdYsWKwyCUKGcBR5fhIFEBabc2ftC1L3_KxQpzlEHenw9kscPH47zKa7GN4AoYibRpk6HdImUmFdjjHLzX4HX4M_qzpejGT6dSFyD38mA-zVj4Pa_vwNw7wIKsRSffwNkjMqNymIC4KLmT8TH4b9LDSzdEbD7-DNbDTBDRpTMPs2cKfBjJGan0wkrCPUflqjUhwKo6duAMya5bxoCVenQAVsgK9nNESdLvbHA7ha6DLDPovKf44qTp8NMr_gmbGH6pyo6uMmtTSMqkVktPC1QwNKGSWUqmCNJ9dRZokrwths11T_ED66Y6leReV8_KzlfL7C_2LdmLmsYdsSEEn1WCL02SHsM_BN7Zk6riECManAkcGYUM674FEuuP8a0sEVw9sizyWZ6ayFwDdc487M_6sV7yjhiwXkjywAD9ClSc3RtXzPeARv9eGTv5VNE67HMwNV-LUenCq0mMG3l3CsRrHtUqbq0oeGR6uI3mmRbXGBunl-bB6Ts55yRiwutimMalC2PHk--JK8OiQ7wmqdPYfKS5U0FukbLkA7lafkyi53so3ah3naY=w800-h396-no)

## Estilização eficiente

_A sigla RSCSS siginifica Reasonable System for CSS Stylesheet Structure, na tradução nos fornece um nome muito sugestível "Sistema Razoável para Estrutura de Estilos de CSS", você notará o quão importante, simples e eficaz que essa metodologia._

**Nomenclatura de components** - componentes **sempre terão pelo menos duas palavras e as palavras sempre em caixa baixa**, sempre separadas por um **hífen**.

```css
// bad
.headerpage {
  /* ... */
}
.--headerpage {
  /* ... */
}
.header---page {
  /* ... */
}
.HEADER-PAGE {
  /* ... */
}

// good
.header-page {
  /* ... */
}
```

**Nomenclatura de elementos** - elementos (filhos de components), **sempre terá apenas uma palavra para esse seletor**.

```css
// bad
.headerpage {
  .headerpage-left {
    /* ... */
  }
  .headerpage-right {
    /* ... */
  }
}

// good
.header-page {
  > .left {
    /* ... */
  }

  > .right {
    /* ... */
  }
}
```

**Elementos de múltiplas palavras** - Para elementos com mais de duas palavras, concatene-os sem os `-` ou `_`.

```css
// bad
.profile-box {
  > .first-name {
    /* ... */
  }
  > .last-name {
    /* ... */
  }
  > .avatar {
    /* ... */
  }
}

// good
.profile-box {
  > .firstname {
    /* ... */
  }
  > .lastname {
    /* ... */
  }
  > .avatar {
    /* ... */
  }
}
```

**Sempre nomeie as tags** - Use `class` sempre, usar tags `[seletor]` há uma performance menor e menos descritiva.

```css
// bad
.header-page {
  [title] {
    /* ... */
  }

  h3 {
    /* ... */
  }
}

// good
.header-page {
  > .title {
    /* ... */
  }
}
```

**Seletores de elementos** - Dê preferência ao seletor `>` ("primeiro filho"), ao selecioná-lo irá prevenir conflitos com outros possíveis componentes ou elementos descendentes ao seletor pai.

```css
.header-page {
  .left {
    /* not wrong */
  }

  > .right {
    /* best way */
  }
}
```

**Nomenclatura de variantes** - Sempre comece com `-`.

```css
// bad
.like-button {
  &.disabled {
    /* ... */
  }

  &.wide {
    /* ... */
  }
}

// good
.like-button {
  &.-disabled {
    /* ... */
  }

  &.-wide {
    /* ... */
  }
}
```

**Nomenclatura de helpers** - Sempre comece com `_` e esteja em um arquivo chamado `helpers`

```css
// bad
.center {
  /* ... */
}

// good
._center {
  /* ... */
}
```

```html
<!-- bad -->
<div class="header-page center"></div>

<!-- good -->
<div class="header-page _center"></div>
```

**Evite propriedades de posicionamento** - Componentes podem ser re-usados em diferentes contextos, se houver alguma propriedade de posicionamento poderá acarretar em componente não reutilizáveis (ou a criação de variantes), defina os posicionamentos nos elementos pais.

Elementos para se evitar:

- Positioning (`position`, `top`, `left`, `right`, `bottom`)
- Floats (`float`, `clear`)
- Margins (`margin`)
- Dimensions (`width`, `height`) \*

```css
// bad
.card-item {
  /* ... */
  width: 150px;
  height: 100px;
  position: absolute;
}

// good
.card-list {
  > .card-item {
    /* ... */
    width: 150px;
    height: 100px;
  }
}
```

## Estrutura de pastas

_A seguir será apresentado a estrutura de pastas que utilizamos e a descrição de cada funcionalidade_

```.
.
+-- public ou build
|   +-- index.html
|   +-- favicon.ico
|   +-- manifest.json
+-- src
|   +-- components
|       -- some-components
|   +-- containers
|       -- some-containers
|   +-- env
|       -- dev.js
|       -- homolog.js
|       -- index.js
|       -- prod.js
|   +-- hocs
|       -- some-hocs
|   +-- layouts
|       -- some-layouts
|   +-- middlewares
|       -- some-middlewares
|   +-- store
|       +-- some-store
|           -- actions.js
|           -- actionTypes.js
|           -- reducer.js
|           -- selectors.js
|       Reducers.js
|       Redux.js
|   +-- utils
|       -- some-utils
|   +-- App.jsx
|   +-- index.js
|   +-- serviceWorker.js
|   +-- styleGlobal.js
+-- .babelrc
+-- ..editorconfig
+-- .eslintrc
+-- .firebaserc
+-- .prettierrc
+-- firebase.json
+-- package.json
```

**public ou build** - Esta pasta é onde ficará o `bundle` da aplicação, onde fica o código compilado e pronta para o usuário final.
**src** - Responsável por abrigar todo o código fonte da aplicação
**src/components** - Responsável para ficar os componentes reutilizáveis
**src/containers** - Responsável por abrigar agrupamentos de components
**src/env** - Responsável por ficar as configurações de cada ambiente, são eles: `dev`, `homolog`, `prod`.
**src/hocs** - Responsável por ficar os `Higher-Order Components`.
**src/layouts** - Responsável por ficar os layouts da aplicação, exemplo: `default`, `blank`;
**src/middlewares** - Responsável por ficar os components que vão ser acionados a partir de uma ação, exemplo: `loading` após fazer uma requisição.
**src/store** - Responsável por abrigar as estruturas do `Redux`.
**src/store/some-store** - `some-estore` = `nome da sua store`, nome da pasta que será sua store no redux
**src/store/some-store/actions** - Arquivo que terá suas `actions creators`
**src/store/some-store/actionTypes** - Arquivo que terá suas `constants` para identificação de `action creators` e `reducers`
**src/store/some-store/reducer** - Arquivo que terá seu `reducer`
**src/store/some-store/selectors** - Arquivo que terá os `mixins` ou `seletores` das propriedades armazenadas na `store`
**src/store/Reducers** - Arquivo que importará todos os `reducers` e será utilizado o `combineReducers` do `redux`
**src/store/Redux** - Arquivo que importará seus `reducers` exportados de `src/store/Reducers`, importará os `middlewares` e criará a sua `store`
**src/utils** - Pasta que terá todas as funções que não se encaixa em um componente em específico e é reaproveitado pela aplicação
**src/App.jsx** - Arquivo que importará seus containers e aplicará as rotas
**src/index.js** - Arquivo que importará seus `App.jsx` e será instanciado o `Provider` do `react-redux`
**src/serviceWorker.js** - Um service worker é um script que seu navegador executa em segundo plano, separado da página da Web
**src/styleGlobal.js** - Estilização global da sua aplicação `React`

**Criação de components**
Para a criação dos seus components, como explicado acima, deverá ser feito dentro de src/components

```
// bad
...src
+-- components
|   +-- mycomponent
|       +-- mycomponent.jsx

// good
+-- components
|   +-- MyComponent
|       +-- index.jsx
|       +-- style.js

// good
+-- components
|   +-- MyComponent
|       +-- assets
|           +-- image.svg
|       +-- index.jsx
|       +-- style.js
```

## Commits Eficientes

_Para haver uma padronização dos commits e não ter aquela história de 'o que tenho que colocar? que prefixo?' ou simplesmente commitar com algo como 'atualizado', temos as convenções a seguir._

**Commitizen** - Commitizen nos ajuda a padronizar e deixar o nosso `commit` mais humano, ou seja, não deixará apenas para a equipe de desenvolvimento saber o que esta escrito e sim para todos os envolvidos no projeto (gestores e etc).

**Instalação do Commitizen** - Abaixo segue passo a passo para a instalação dessa ferramenta

```jsx
// Instale o commitizen de forma global
npm i -g commitizen

// ... após a instalação da cli, instale o changelog convencional
npm install -g cz-conventional-changelog

// ... após a instalação do changelog, você irá iniciar o commitizen na pasta raiz do seu projeto
// você pode utilizar via `npm`
commitizen init cz-conventional-changelog --save-dev --save-exact
// ou utilizar o `yarn`
commitizen init cz-conventional-changelog --yarn --dev --exact
```

Após a instalação, você poderá adicionar a sua mudança normalmente, exceto quando for commitar.
`git add -A`
`git cz` <- Nesta etapa irá abrir o changelog default para escolher a forma de commit, conforme a imagem abaixo
![https://lh3.googleusercontent.com/l7ujwcz9oxL9I-eV8fV6qHmQibtbazY1n6hhAjER946GFaMJehsKDSLF-5Z4h77PKsvDCGyqTrR5v4oh1U-WNMJ8IT9_XZ5ptL1ox1QfJaa4euH4HrxkFIBBjRxKtaVPwnGqqzgmD_VI0AEolKR3-Hr0DuURIJc0tPqZzp4FSYkdI7UvISZeohsWBZ-cK3yC4alm7gz_A3_XAZsHV2D3F6S_JN-E172T1sZ87eU9xzIR-4_RsMFYh-DrFLWvehqKQ0OLYOpkECrlif_jGofG-9s5pdjCVe9VJBxIWvrWC4vOqMEz4ho8Bp6RlfT5RRq0_fi7jvlN2M_ssdMHcsdTilskCR11414ztS69VMPWq7oyjbzg7ivmUnwKKdtN6XxP9UA_QZVgYvKuArMqu4VU2IRU6NZQAp4Nwxd4KvPS5SzFL8vb6MJH7eoyU7mw5RyO7W3s48PcHMTUpYOqHZ1r55HhvKX3vyckdVMCebyomOq-LxSkpzolxC4FWLCrkgtXLgLTQ_oZCjw-KYlqRkqtSbEwzG0ftwLSEPhl5BKeD2KRF2hnOEd4GaTqHiwlkyOb_3Ezqk1zpxLTnS2pin0U_vvAhgVo-vWeRO9me2eucEtPM6dLuybfCR0fh3qqLlxNijiTJos5LtzZO8PKP-0wuJgLR0jNxQ=w987-h523-no](https://lh3.googleusercontent.com/l7ujwcz9oxL9I-eV8fV6qHmQibtbazY1n6hhAjER946GFaMJehsKDSLF-5Z4h77PKsvDCGyqTrR5v4oh1U-WNMJ8IT9_XZ5ptL1ox1QfJaa4euH4HrxkFIBBjRxKtaVPwnGqqzgmD_VI0AEolKR3-Hr0DuURIJc0tPqZzp4FSYkdI7UvISZeohsWBZ-cK3yC4alm7gz_A3_XAZsHV2D3F6S_JN-E172T1sZ87eU9xzIR-4_RsMFYh-DrFLWvehqKQ0OLYOpkECrlif_jGofG-9s5pdjCVe9VJBxIWvrWC4vOqMEz4ho8Bp6RlfT5RRq0_fi7jvlN2M_ssdMHcsdTilskCR11414ztS69VMPWq7oyjbzg7ivmUnwKKdtN6XxP9UA_QZVgYvKuArMqu4VU2IRU6NZQAp4Nwxd4KvPS5SzFL8vb6MJH7eoyU7mw5RyO7W3s48PcHMTUpYOqHZ1r55HhvKX3vyckdVMCebyomOq-LxSkpzolxC4FWLCrkgtXLgLTQ_oZCjw-KYlqRkqtSbEwzG0ftwLSEPhl5BKeD2KRF2hnOEd4GaTqHiwlkyOb_3Ezqk1zpxLTnS2pin0U_vvAhgVo-vWeRO9me2eucEtPM6dLuybfCR0fh3qqLlxNijiTJos5LtzZO8PKP-0wuJgLR0jNxQ=w987-h523-no)

Cada ítem há uma função:

- **feat:** Feature nova;
- **fix:** Correção de código;
- **docs:** Melhoria de comentário de código ou edição da parte de documentação da sua base de código;
- **style:** melhorias de formatação de texto, mudança de estilo de codificação;
- **refactor:** Uma vez trabalhando com boa cobertura de teste ou não, esse commit é quando for apenas refatorar seu código;
- **perf:** Quase mesma pegada do refactor mas esse ajuste resulta em maior perfomance da solução;
- **test:** Inclusão ou manutenção de testes do software;
- **chore:** Se o que alterou não está associado a algum listado acima, utilize ele para classificar, (utilizado para terminar alguma tarefa).

Após selecionar um item do commitizen, irá aparecer alguns passos a serem preenchidos

![https://lh3.googleusercontent.com/PeymXB_8nmAfdu0KjBUIg6ytCbxdNObfrLQoxcqd4fp0ZliUgGGIsKN0dDN0fgHbN5v6ZPZPwaJW5Z3PXVXh2ew0O2myFcuP5wgbg1imHpWCVkRxyXDESz_vpinrq-1Orfe5fn7M5Wr6c7MOmTFvmUsjHhmkltfXHv9YT9AgDrgi-7v6Of879fRF3JCQ8ectT8f8Focth9j9Y_2Y5FphgDhKVDwLpm3-0XlPnTkbCGCMxhkxeVU5Sa8rIa9x5UqI9BGVetgQyItI3gf0y0l-7UjR70Nnu0aRxQ0iGkaaqdvCQzp84vuXhvh9w5sxtXGIxLmY516CEjKhHfK6cNiqESEsfuPkADNrFrMpeJGDkOIfsPSSSY2JDg6sObvqGmacgmJn03VKdSdAb4YvHK2M5u7MmD9QOwDGINnmCHjs0ujiCDApZLWKZTKOR4gB7FFop_qUvTxrF8FRRJJ6ThAUIljlG6Q4xzdPzDVSUTwBx1YJCyVPax0rNu0lt0lEUrMFBVyOLKof0xsVvLThzLkp5i5hvVWvUY0oUlKHelf27n-dynG8j1mUiXuo2FJaat83sOpNoJCZ54cuX4m_3zAzdgOsKhbRVG5-GGUTmafbkAZC1k5DzGLzRbEDY5MxOD50niIJB1qlmBRKRz9eQfrEhPqWAqPb5Q=w1104-h545-no](https://lh3.googleusercontent.com/PeymXB_8nmAfdu0KjBUIg6ytCbxdNObfrLQoxcqd4fp0ZliUgGGIsKN0dDN0fgHbN5v6ZPZPwaJW5Z3PXVXh2ew0O2myFcuP5wgbg1imHpWCVkRxyXDESz_vpinrq-1Orfe5fn7M5Wr6c7MOmTFvmUsjHhmkltfXHv9YT9AgDrgi-7v6Of879fRF3JCQ8ectT8f8Focth9j9Y_2Y5FphgDhKVDwLpm3-0XlPnTkbCGCMxhkxeVU5Sa8rIa9x5UqI9BGVetgQyItI3gf0y0l-7UjR70Nnu0aRxQ0iGkaaqdvCQzp84vuXhvh9w5sxtXGIxLmY516CEjKhHfK6cNiqESEsfuPkADNrFrMpeJGDkOIfsPSSSY2JDg6sObvqGmacgmJn03VKdSdAb4YvHK2M5u7MmD9QOwDGINnmCHjs0ujiCDApZLWKZTKOR4gB7FFop_qUvTxrF8FRRJJ6ThAUIljlG6Q4xzdPzDVSUTwBx1YJCyVPax0rNu0lt0lEUrMFBVyOLKof0xsVvLThzLkp5i5hvVWvUY0oUlKHelf27n-dynG8j1mUiXuo2FJaat83sOpNoJCZ54cuX4m_3zAzdgOsKhbRVG5-GGUTmafbkAZC1k5DzGLzRbEDY5MxOD50niIJB1qlmBRKRz9eQfrEhPqWAqPb5Q=w1104-h545-no)

Passos:

- What is the scope of this change (e.g component or file name)?
- (Tradução) Qual o escopo que ocorreu a mudança?
  - Nome-da-feature (ou a tarefa que você esta fazendo)
- Write a short, imperative tense description of the change:
- Escreva uma pequena sentença no imperativo sobre a mudança:
  - Crie a feature (geralmente é o nome da task)
- Provide a longer description of the change:
- Descreva uma descrição longa para a mudança:
  - Criada a feature X, adicionando as seguintes operações: Y = A + B (descrever adequadamente o que você acabou de fazer)

As outras duas opções são opcionais
